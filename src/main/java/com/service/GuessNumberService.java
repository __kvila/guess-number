package main.java.com.service;

import java.util.Random;
import java.util.Scanner;

public class GuessNumberService {
    private static final String MSG_SELECT_RANGE = "Select a range...";
    private static final String MSG_INVALID_MIN = "Invalid minimum!\nYou cannot select a minimum value below 1 or above 200.";
    private static final String MSG_INVALID_MAX = "Invalid minimum!\nYou cannot select a maximum value below or equal to minimum or above 200.";
    private static final String MSG_SELECT_MINIMUM = "Select minimum: ";
    private static final String MSG_SELECT_MAXIMUM = "Select maximum: ";
    private static final String MSG_SELECT_ATTEMPTS = "Enter the number of attempts: ";
    private static final String MSG_INVALID_ATTEMPTS = "Invalid number!\nYou cannot select the number of attempts below 1 or more than 15.";
    private static final String MSG_EXIT = "The application is closed. Bye!";
    private static final String MSG_CONGRATULATIONS = "Congratulations! You guessed the number!";
    private static final String MSG_NO_ATTEMPTS = "You have no attempts left.";
    private static final String MSG_VERY_HOT = "Very hot!";
    private static final String MSG_HOT = "Hot...";
    private static final String MSG_VERY_COLD = "Very cold!";
    private static final String MSG_COLDLY = "Coldly.";
    private static final String MSG_INVALID_ANSWER = "You cannot enter a number that is less than the minimum or greater than the maximum!";
    private static final String RESPONSE_EXIT = "exit";

    private static int min, max, attempts;

    private final Scanner scanner;
    private int hiddenNumber;
    private final Random random;
    private int half;

    public GuessNumberService(Scanner scanner, Random random){
        this.scanner = scanner;
        this.random = random;

        System.out.println(MSG_SELECT_RANGE);
        chooseRange();

        randomHiddenNumber();

        chooseAttempts();
    }

    public void randomHiddenNumber(){
        while(true) {
            hiddenNumber = random.nextInt(max);
            if(hiddenNumber < min || hiddenNumber > max){
                continue;
            }
            break;
        }
    }

    public void startGuessing(){
        System.out.println("Hi, I am guessing a number from " + min +  " to " + max + " of your range. Try to guess it in " + attempts + " tries!");
        checkAnswer();
    }

    public void checkAnswer(){
        int answer;

        while(true) {
            if (attempts == 0) {
                System.out.println(MSG_NO_ATTEMPTS);
                break;
            }

            answer = Integer.parseInt(scanResponse());

            if (answer == hiddenNumber) {
                System.out.println(MSG_CONGRATULATIONS);
                break;
            } else if (answer < min || answer > max) {
                System.out.println(MSG_INVALID_ANSWER);
            } else {
                chooseWarmerColder(answer);
            }
        }
    }

    public void chooseWarmerColder(int answer){
        int difference = hiddenNumber - answer;
        if((difference <= 5) && (difference >= -5)){
            System.out.println(MSG_VERY_HOT);
        } else
            if((difference <= 10) && (difference >= -10)){
                System.out.println(MSG_HOT);
            } else
                if((difference <= 20) && (difference >= -20)){
                    System.out.println(MSG_COLDLY);
                } else
                    if((difference > 20) || (difference < -20)){
                        System.out.println(MSG_VERY_COLD);
                    }
        attempts--;
        if(attempts == 0){
            return;
        } else {
            System.out.println("You have " + attempts + " attempts");
        }
    }

    public void chooseAttempts(){
        System.out.println(MSG_SELECT_ATTEMPTS);
        while(true) {
            attempts = Integer.parseInt(scanResponse());
            if(attempts < 1 || attempts > 15){
                System.out.println(MSG_INVALID_ATTEMPTS);
                continue;
            }
            break;
        }
    }

    public void chooseRange(){
        while(true){
            System.out.println(MSG_SELECT_MINIMUM);
            min = Integer.parseInt(scanResponse());

            if(min < 1 || min > 200){
                System.out.println(MSG_INVALID_MIN);
                continue;
            }
            break;
        }

        while(true){
            System.out.println(MSG_SELECT_MAXIMUM);
            max = Integer.parseInt(scanResponse());

            if(max <= min || max > 200){
                System.out.println(MSG_INVALID_MAX);
                continue;
            }
            break;
        }
    }

    public String scanResponse(){
        String response = scanner.nextLine();

        if(response.equalsIgnoreCase(RESPONSE_EXIT)){
            System.out.println(MSG_EXIT);
            System.exit(0);
        }

        return response;
    }
}

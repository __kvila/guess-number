package main.java.com;

import main.java.com.service.GuessNumberService;

import java.util.Random;
import java.util.Scanner;

public class GuessNumberRun {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        new GuessNumberService(scanner, random).startGuessing();
    }
}
